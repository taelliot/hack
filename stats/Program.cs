﻿using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TA.Metrics.Configuration;
using TA.Metrics.Implementation.Statsd;
using TA.Metrics.Interface;

namespace stats {
    class Program {
        static void Main(string[] args) {
            IOptions<StatsOptions> opts = Options.Create<StatsOptions>(new StatsOptions {
                Enabled = true,
                    Host = "stats.devalertlist.com",
                    Port = 30125,
                    UseTCP = false,
                    Prefix = "etest",
                    MaxUDPPacketSize = 512
            });
            var lf = new LoggerFactory();
            var metrics = new StatsDMetricsReporter(opts, lf.CreateLogger<IMetricsReporter>());
            Console.WriteLine("Hello World!");
            for (var hc = 0; hc < 5; hc++) {
                for (var x = 0; x < 100; x++) {
                    metrics.Counter($"test#host=host{hc}test,env=qa");
                    metrics.Counter($"test#host=host{hc}test,env=dev");
                }
            }
        }
    }
}