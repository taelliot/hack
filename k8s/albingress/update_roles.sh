#!/bin/sh
cluster_name=$(kubectl cluster-info | awk 'NR<2{print $6}' | sed -e "s|https://api.||g")
policy_name=k8s-alb-creator

if [ -z ${cluster_name} ]; then
    echo "Could not find a cluster using kubectl cluster-info"
    exit 1
fi

echo "Setting up  cluster ${cluster_name} to do alb work"
# remove all the color garbage
cluster_name=$(echo "${cluster_name}" | sed -E "s/"$'\E'"\[([0-9]{1,3}((;[0-9]{1,3})*)?)?[m|K]//g")

masters_role=masters.${cluster_name}
nodes_role=nodes.${cluster_name}
policy_arn=$(aws iam list-policies | grep ${policy_name} | awk '{print $2'})
if [ -z ${policy_arn} ]; then
    echo "Creating IAM Role ${policy_name}..."
    aws iam create-policy --policy-name ${policy_name} --policy-document file://iam_role.json
fi
policy_arn=$(aws iam list-policies | grep ${policy_name} | awk '{print $2'})

echo "Attaching ${policy_arn} to ${masters_role} and ${nodes_role}"
aws iam attach-role-policy --role-name ${masters_role} --policy-arn=${policy_arn}
aws iam attach-role-policy --role-name ${nodes_role} --policy-arn=${policy_arn}
 
